
from flask import Flask, jsonify, request, abort, redirect, url_for
from flask_restful import Api, Resource, reqparse
from bson.son import SON
from bson.objectid import ObjectId
import json
import numpy as np
from pandas import read_csv
import pymongo
from pymongo import MongoClient
import time
from networkx import Graph, connected_components
import random



def create_mongodb(databasename='news_scrap_db2', collection_name = 'news2'):
    #Creating a pymongo client
    client = MongoClient("mongodb://user2:user2password@139.99.45.55/")#MongoClient('localhost', 27017)
    #client = MongoClient("mongodb://user2:user2password@0.0.0.0:27017/")#MongoClient('localhost', 27017)

    # 172.17.0.1 can be used to communicate to the local server of mongodb, 
    # tried it but nno change in loading time.

    #Getting the database instance
    db = client[databasename]
    print("Database created........")
    print(client.list_database_names())
    
    collection = db[collection_name]
    print("Collection created........")
    
    collection.create_index([ ("url", pymongo.ASCENDING) ], unique = True)
    return client, db, collection

#Initialise
client, db, collection = create_mongodb(databasename='news_scrap_db2', collection_name = 'news2')



def create_mongodb_noidx(databasename='news_scrap_db2', collection_name = 'news2'):
    #Creating a pymongo client
    client = MongoClient("mongodb://user2:user2password@139.99.45.55/")#MongoClient('localhost', 27017)
    #client = MongoClient("mongodb://user2:user2password@127.0.0.1:27017/")#MongoClient('localhost', 27017)
    

    #Getting the database instance
    db = client[databasename]
    print("Database created........")
    print(client.list_database_names())
    
    collection = db[collection_name]
    print("Collection created........")
    
    return client, db, collection

#Initialise
client, db, collection_digest = create_mongodb_noidx(databasename='news_scrap_db2', collection_name = 'news_digest')


#Load topic subtopic mapping in variable
with open("./topic_subtopic_map.json", "r") as fp:   # Unpickling
    topic_subtopic_map = json.load(fp)

#Load section topic subtopic mapping in variable
with open("./sect_topic_subtopic_map.json", "r") as fp:   # Unpickling
    sect_topic_subtopic_map = json.load(fp)

# with open("./sect_topic_map.json", "r") as fp:   # Unpickling
#     sect_topic_map = json.load(fp)

with open("./sect_topic_image_map.json", "r") as fp:   # Unpickling
    sect_topic_image_map = json.load(fp)

#import transformers
app = Flask(__name__)
api = Api(app)

#Graph Helper

def create_graph(news_list):
    '''
        Get Nodes and edges using "similar_id" field.
        create a graph using nodes and edges.
    '''
    nodes = []
    edges = []

    for o in news_list:
        nodes.append(o["_id"])
        for e in o["similar_id"]:
            edg = (o["_id"], e)
            edges.append(edg)
    
    G = Graph()
    G.add_nodes_from(nodes)
    G.add_edges_from(edges)

    return G

def get_unique_news_id(G):
    '''
        Pass list of sets where each set is a connected component of Graph.
        select 1 news from connected components.
    '''
    cc_id_list = list(connected_components(G))
    #unique_id_list = [s.pop() for s in cc_id_list]
    unique_id_list = []
    for s in cc_id_list:
        #print(s)
        #print(type(s))
        temp = random.sample(s, k = 1)[0]
        #print(temp)
        unique_id_list.append(temp)

    return unique_id_list

def get_unique_news_id_following(G, follow_pub_list, id_scource_mapping):
    '''
        Pass list of sets where each set is a connected component of Graph.
        select 1 news from connected components.
        But selection should be done iff the publisher is mentioned in `follow_pub_list`.
    '''

    #the list of fllowing publisher is not given then don't apply filter by publisher
    if follow_pub_list == []:
        return get_unique_news_id(G)


    cc_id_list = list(connected_components(G))
    unique_id_list = []

    for id_set in cc_id_list:
        for id in id_set:
            if id_scource_mapping.get(id) in follow_pub_list:
                unique_id_list.append(id)
                break

    return unique_id_list


def get_news_by_rscore(unique_id_list, skip, limit, gravity = 1.1):
    '''
        Given list of news id.
        Sort them by `r_score` and return the cursor for the query.
        `gravity` : float (default = 1.8)
    '''

    cur_time = int(time.time())
    news_list_cursor = collection.aggregate(
    [
        {
            "$match": { "_id" : { "$in" : unique_id_list } }
        },
        { 
            "$project": { 
                "title":1,              # choose field to return
                "img_src": 1,
                "primary_summary": 1,
                "url":1,
                "date":1,
                "authors":1,
                "source_name":1,
                "likes_count":1,
                "views_count":1,
                "share_count":1,
                "scrap_time":1,
                "video_src":1,
                "tags":1,
                "subtopics":1,
                'dislikes_count': 1,
                'laughing_count': 1,
                'surprise_count': 1,
                'confuse_count': 1,
                'straight_count': 1,
                'angry_count': 1,
                "comments_count": 1,
                "similar_id":1,
                "topics":1,
                "r_score": {            #calculating relevancy score (r_score)
                    "$divide": [ 
                                {
                                    "$add": [ 
                                        "$likes_count",
                                        "$laughing_count",
                                        "$surprise_count",
                                        "$confuse_count",
                                        "$straight_count",
                                        "$angry_count",
                                        "$share_count",
                                        "$comments_count",
                                        10,
                                        {
                                            "$multiply": ["$views_count", 2]#"$divide": ["$views_count", 5]
                                        }
                                        ]
                                } ,
                                {
                                    "$pow": [
                                            gravity,
                                            {"$add": [ {"$divide": [ {"$subtract": [cur_time , "$scrap_time"]}, 3600 ]}, 2]}                                           
                                            ]
                                }   
                                ]  
                            } 
                        } 
        },
        {
            "$sort": SON( [("r_score",-1), ("scrap_time", -1)] ) 
        },
        {
            "$skip" : skip
        },
        {
            "$limit" : limit
        }
    ]
    )    

    return news_list_cursor



#NLTK STOPWORD 
from nltk.corpus import stopwords 
from nltk.tokenize import word_tokenize 
import nltk
nltk.download('stopwords')
nltk.download('punkt')
import re
stop_words = set(stopwords.words('english')) 

def remove_stopwords(example_sent):  
    #example_sent = "This is a sample sentence, showing off the stop words filtration."

    example_sent = example_sent.lower()
    example_sent = re.sub(r"[^a-zA-Z0-9]+", ' ', example_sent)
    #print(example_sent)

    word_tokens = word_tokenize(example_sent) 


    filtered_sentence = [] 

    for w in word_tokens: 
        if w not in stop_words and len(w) > 2: 
            filtered_sentence.append(w) 

    filtered_sentence = " ".join(filtered_sentence)
    return filtered_sentence


def check_auth(username, password):
    return username == 'admin' and password == 'secret'

def authenticate_check(request_obj):
    auth = request_obj.authorization
    if not auth or not check_auth(auth.username, auth.password):
        return False
    else:
        return True



class Home(Resource):
    def get(self):


        if not authenticate_check(request):
            retJSON = {
                'error': "Invalid Authentication."
            }

            return jsonify(retJSON) 


        retJSON = {
            'message':"Hello, Working",
            'Status Code':200,  
        }

        return jsonify(retJSON)  


def RecentNews_helper(page, prev_news_id = []):
    skip = 0 + 20 * ( page - 1)
    limit = 20

    # Request for recent articles, i.e. latest articles by scrap_time.
    # Store the mongo query in old.
    # Limit is set so that we don't perform a huge graph traversal.
    try:
        old = collection.find(
            {
                "similarity_flag": 1,       #select  those with similarity and summary are generated
                "summarized_flag": 1,       # and primary summary exist.
                "primary_summary": { "$ne": None}
            },      
            {"similar_id":1},
            sort = [("_id", -1)],    #[("scrap_time", -1)],
            limit = 2500)
        old = [o for o in old]
    except:
        return abort(501, "Server Error. Can't get 'old' data from DB. ")


    #Graph and unique id list
    try:
        G = create_graph(old)
    except:
        return abort(501, "Server Error. Error in Graph creation ")

    try:
        unique_id_list = get_unique_news_id(G)
    except:
        return abort(501, "Server Error. Error in getting connected components. ")


    try:
        news_list_cursor = collection.find(
            { "_id" : { "$in" : unique_id_list } }, # get news data from unique list.
            {
                "title":1,              # choose field to return
                "img_src": 1,
                "primary_summary": 1,
                "url":1,
                "date":1,
                "authors":1,
                "source_name":1,
                "likes_count":1,
                "views_count":1,
                "share_count":1,
                "scrap_time":1,
                "tags":1,
                "subtopics":1,
                "video_src":1,
                'dislikes_count': 1,
                'laughing_count': 1,
                'surprise_count': 1,
                'confuse_count': 1,
                'straight_count': 1,
                'angry_count': 1,
                "comments_count":1,
                "similar_id":1,
                "topics":1,

            },
            sort=[("_id", -1)],      #sort by scrap time for recent news.
            skip = skip,                    
            limit = limit
            )
    except:
        return abort(501, "Server Error. Error in querying news list. Check DB. ")


    news_list = []

    for news in news_list_cursor:
        news["_id"] = str(news["_id"])

        if news["_id"] not in prev_news_id:
            news["similar_id"] = [str(n_id) for n_id in news["similar_id"]]
            news_list.append(news)



    retJSON = {
        "metadata":{
            "page":page,
            "news_count": len(news_list)
        },
        "news": news_list
    }
    #print(retJSON)
    return jsonify(retJSON)


class RecentNews(Resource):
    def get(self):
        
        if not authenticate_check(request):
            retJSON = {
                'error': "Invalid Authentication."
            }

            return jsonify(retJSON) 

        # get page for skip and limit
        try:
            page = int(request.args.get('page'))
        except:
            return abort(400, "Error. See if page number is correctly given.")

        try:
            prev_news_id = request.args.getlist('prev_news_id')

            if not isinstance(prev_news_id, list):
                abort(400, "Error. See if prev_news_id is array or list [].")
        except:
            return abort(400, "Error. See if prev_news_id is correctly given.")

        #print("type: ", type(page))
        #print("page: ", page)

        retJSON = RecentNews_helper(page, prev_news_id)

        return retJSON
  



class Hot(Resource):
    def get(self):
        
        if not authenticate_check(request):
            retJSON = {
                'error': "Invalid Authentication."
            }

            return jsonify(retJSON) 

        # get page for skip and limit
        try:
            page = int(request.args.get('page'))
        except:
            return abort(400, "Error. See if page number is correctly given.")
        try:
            prev_news_id = request.args.getlist('prev_news_id')

            if not isinstance(prev_news_id, list):
                abort(400, "Error. See if prev_news_id is array or list [].")
        except:
            return abort(400, "Error. See if prev_news_id is correctly given.")

        skip = 0 + 20 * ( page - 1)
        limit = 20

        # Request for recent articles, i.e. latest articles by scrap_time.
        # Store the mongo query in old.
        # Limit is set so that we don't perform a huge graph traversal.
        try:
            old = collection.find(
                {
                    "similarity_flag": 1,       #select  those with similarity and summary are generated
                    "summarized_flag": 1,       # and primary summary exist.
                    "primary_summary": { "$ne": None}
                },      
                {"similar_id":1},
                sort = [("_id", -1)],
                limit = 2500)
            old = [o for o in old]
        except:
            return abort(501, "Server Error. Can't get 'old' data from DB. ")


        #Graph and unique id list
        try:
            G = create_graph(old)
        except:
            return abort(501, "Server Error. Error in Graph creation ")

        try:
            unique_id_list = get_unique_news_id(G)
        except:
            return abort(501, "Server Error. Error in getting connected components. ")


        try:
            news_list_cursor = get_news_by_rscore(unique_id_list, skip, limit)
        except:
            return abort(501, "Server Error. Error in querying news list. Check DB. ")
 
 
 
        news_list = []
        tags_list = []

        for news in news_list_cursor:
            news["_id"] = str(news["_id"])
            if news["_id"] not in prev_news_id:
                news["similar_id"] = [str(n_id) for n_id in news["similar_id"]]
                news_list.append(news)
                try:
                    #tags_list.append(random.choice(news["tags"])) #choice for 1
                    tags_list.append(news["tags"][0]) #choice for 1

                except:
                    pass

        #news_tags_selected = random.sample(tags_list, k= 8)   #choices for multiple
        if page == 1:
            news_tags_selected = tags_list[:10]
        else:
            news_tags_selected = []
        
        retJSON = {
            "metadata":{
                "page":page,
                "news_count": len(news_list),
                "news_tags": news_tags_selected
            },
            "news": news_list
        }
        #print(retJSON)
        return jsonify(retJSON)  



class Following(Resource):
    def post(self):
        # write what to do for post request and Add class
        #Load the data
        postedData = request.get_json()


        if not authenticate_check(request):
            retJSON = {
                'error': "Invalid Authentication."
            }
            return jsonify(retJSON)        

        #Validation
        try:
            follow_pub_list = postedData['follow_pub_list']
        except:
            return abort(400, "Error in publishers list. Enter array [].")
        try:
            follow_topic_list = postedData['follow_topic_list']
        except:
            return abort(400, "Error in topic list. Enter array [].")

        try:
            page =  int(postedData['page'])
        except:
            return abort(400, "Error. See if page number is correctly given.")

        try:
            prev_news_id = postedData.get('prev_news_id')

            if not isinstance(prev_news_id, list):
                #abort(400, "Error. See if prev_news_id is array or list [].")
                prev_news_id = []

        except:
            return abort(400, "Error. See if prev_news_id is correctly given.")


        #get subtopic mapping
        if follow_topic_list != None:
            follow_subtopic_list = []
            for top in follow_topic_list:
                subtop = topic_subtopic_map.get(top)
                if subtop != None:
                    follow_subtopic_list.extend(subtop)

        
            
        
        #skip and limit calculation
        skip = 0 + 20 * ( page - 1)
        limit = 20

        # Request for recent articles, i.e. latest articles by scrap_time.
        # Store the mongo query in old.
        # Limit is set so that we don't perform a huge graph traversal.
        if follow_pub_list != [] and follow_subtopic_list != []:
            try:
                old_follow = collection.find(
                    {
                        "similarity_flag": 1,       #select  those with similarity and summary are generated
                        "summarized_flag": 1,       # and primary summary exist.
                        "primary_summary": { "$ne": None},
                        "source_name": { "$in" : follow_pub_list },         #list of scource name to get only following publishers articles.
                        "subtopics": { "$in" : follow_subtopic_list }         #list of scource name to get only following publishers articles.
                        
                    },      
                    {
                        "similar_id":1,
                        "source_name": 1
                    },
                    sort = [("_id", -1)],
                    limit = 2500)
                
                old = []
                id_scource_mapping = {}
                for o in old_follow:
                    old.append(o)
                    id_scource_mapping[o["_id"]] = o["source_name"]

            except:
                return abort(501, "Server Error. Can't get 'old' data from DB. ")
        elif follow_pub_list != [] and follow_subtopic_list == []:
            try:
                old_follow = collection.find(
                    {
                        "similarity_flag": 1,       #select  those with similarity and summary are generated
                        "summarized_flag": 1,       # and primary summary exist.
                        "primary_summary": { "$ne": None},
                        "source_name": { "$in" : follow_pub_list },         #list of scource name to get only following publishers articles.
                        #"subtopics": { "$in" : follow_subtopic_list }         #list of scource name to get only following publishers articles.
                        
                    },      
                    {
                        "similar_id":1,
                        "source_name": 1
                    },
                    sort = [("_id", -1)],
                    limit = 2500)
                
                old = []
                id_scource_mapping = {}
                for o in old_follow:
                    old.append(o)
                    id_scource_mapping[o["_id"]] = o["source_name"]

            except:
                return abort(501, "Server Error. Can't get 'old' data from DB. ")        
        elif follow_pub_list == [] and follow_subtopic_list != []:
            try:
                old_follow = collection.find(
                    {
                        "similarity_flag": 1,       #select  those with similarity and summary are generated
                        "summarized_flag": 1,       # and primary summary exist.
                        "primary_summary": { "$ne": None},
                        #"source_name": { "$in" : follow_pub_list },         #list of scource name to get only following publishers articles.
                        "subtopics": { "$in" : follow_subtopic_list }         #list of scource name to get only following publishers articles.
                        
                    },      
                    {
                        "similar_id":1,
                        "source_name": 1
                    },
                    sort = [("scrap_time", -1)],
                    limit = 2500)
                
                old = []
                id_scource_mapping = {}
                for o in old_follow:
                    old.append(o)
                    id_scource_mapping[o["_id"]] = o["source_name"]

            except:
                return abort(501, "Server Error. Can't get 'old' data from DB. ")        
        else:
            try:
                old_follow = collection.find(
                    {
                        "similarity_flag": 1,       #select  those with similarity and summary are generated
                        "summarized_flag": 1,       # and primary summary exist.
                        "primary_summary": { "$ne": None},
                        #"source_name": { "$in" : follow_pub_list },         #list of scource name to get only following publishers articles.
                        #"subtopics": { "$in" : follow_subtopic_list }         #list of scource name to get only following publishers articles.
                        
                    },      
                    {
                        "similar_id":1,
                        "source_name": 1
                    },
                    sort = [("scrap_time", -1)],
                    limit = 2500)
                
                old = []
                id_scource_mapping = {}
                for o in old_follow:
                    old.append(o)
                    id_scource_mapping[o["_id"]] = o["source_name"]

            except:
                return abort(501, "Server Error. Can't get 'old' data from DB. ")        

            


        #Graph and unique id list
        try:
            G = create_graph(old)
        except:
            return abort(501, "Server Error. Error in Graph creation ")

        try:
            unique_id_list = get_unique_news_id_following(G, follow_pub_list,id_scource_mapping )
        except:
            return abort(501, "Server Error. Error in getting connected components. ")


        try:
            news_list_cursor = get_news_by_rscore(unique_id_list, skip, limit)
        except:
            return abort(501, "Server Error. Error in querying news list. Check DB. ")
 
 
 
        news_list = []

        for news in news_list_cursor:
            news["_id"] = str(news["_id"])
            if news["_id"] not in prev_news_id:
                news["similar_id"] = [str(n_id) for n_id in news["similar_id"]]
                news_list.append(news)

        
        retJSON = {
            "metadata":{
                "page":page,
                "news_count": len(news_list)
            },
            "news": news_list
        }
        #print(retJSON)
        return jsonify(retJSON)  






class Video(Resource):
    def get(self):
        
        '''
        Get best video based on `r_score`.
        '''

        #check authentication
        if not authenticate_check(request):
            retJSON = {
                'error': "Invalid Authentication."
            }
            return jsonify(retJSON)     

        # get page for skip and limit
        try:
            page = int(request.args.get('page'))
        except:
            return abort(400, "Error. See if page number is correctly given.")

        #previous news id
        try:
            prev_news_id = request.args.getlist('prev_news_id')

            if not isinstance(prev_news_id, list):
                abort(400, "Error. See if prev_news_id is array or list [].")
        except:
            return abort(400, "Error. See if prev_news_id is correctly given.")


        skip = 0 + 45 * ( page - 1)
        limit = 45

        # Request for recent articles, i.e. latest articles by scrap_time.
        # Store the mongo query in old.
        # Limit is set so that we don't perform a huge graph traversal.
        try:
            old = collection.find(
                {
                    "similarity_flag": 1,       #select  those with similarity and summary are generated
                    "summarized_flag": 1,       # and primary summary exist.
                    "primary_summary": { "$ne": None},
                    "video_src": {"$ne": []}        #news with video links
                },      
                {"_id":1, "similar_id": 1},
                sort = [("scrap_time", -1)],
                limit = 2500)
            old = [o for o in old] # only take id
        except:
            return abort(501, "Server Error. Can't get 'old' data from DB. ")

        #Graph and unique id list
        try:
            G = create_graph(old)
        except:
            return abort(501, "Server Error. Error in Graph creation ")

        try:
            unique_id_list = get_unique_news_id(G)
        except:
            return abort(501, "Server Error. Error in getting connected components. ")




        video_id_list  = unique_id_list

        try:
            video_list_cursor = get_news_by_rscore(video_id_list, skip, limit)
        except:
            return abort(501, "Server Error. Error in querying news list. Check DB. ")
 
 
 
        video_news_list = []
        video_src_list = set()     #to remove similar news with same video.

        for vid in video_list_cursor:
            vid["_id"] = str(vid["_id"])
            if vid["_id"] not in prev_news_id and vid["video_src"] != []:
                if vid["video_src"][0] not in video_src_list:
                    video_src_list.add(vid["video_src"][0])
                    vid["similar_id"] = [str(n_id) for n_id in vid["similar_id"]]
                    video_news_list.append(vid)

        
        retJSON = {
            "metadata":{
                "page":page,
                "news_count": len(video_news_list)
            },
            "news": video_news_list
        }
        #print(retJSON)
        return jsonify(retJSON)  


class Video_2(Resource):
    def get(self):
        
        '''
        Get best video based on `r_score`.
        '''

        #check authentication
        if not authenticate_check(request):
            retJSON = {
                'error': "Invalid Authentication."
            }
            return jsonify(retJSON)     

        # get page for skip and limit
        try:
            page = int(request.args.get('page'))
        except:
            return abort(400, "Error. See if page number is correctly given.")

        #previous news id
        try:
            prev_news_id = request.args.getlist('prev_news_id')

            if not isinstance(prev_news_id, list):
                abort(400, "Error. See if prev_news_id is array or list [].")
        except:
            return abort(400, "Error. See if prev_news_id is correctly given.")


        skip = 0 + 10 * ( page - 1)
        limit = 10

        # Request for recent articles, i.e. latest articles by scrap_time.
        # Store the mongo query in old.
        # Limit is set so that we don't perform a huge graph traversal.
        try:
            old = db.youtube_video.find(
                {},
                skip = skip,      
                limit = limit,
                sort = [("publishedAt", -1)])
            old = [o for o in old] # only take id
        except:
            return abort(501, "Server Error. Can't get 'old' data from DB. ")

        for o in old:
            o["_id"] = str(o["_id"])

        retJSON = {
            "metadata":{
                "page":page,
                "news_count": len(old)
            },
            "news": old
        }
        #print(retJSON)
        return jsonify(retJSON)  




class Covid(Resource):
    def get(self):
        
        #check authentication
        if not authenticate_check(request):
            retJSON = {
                'error': "Invalid Authentication."
            }
            return jsonify(retJSON)     

        # get page for skip and limit
        try:
            page = int(request.args.get('page'))
        except:
            return abort(400, "Error. See if page number is correctly given.")

        try:
            prev_news_id = request.args.getlist('prev_news_id')

            if not isinstance(prev_news_id, list):
                abort(400, "Error. See if prev_news_id is array or list [].")
        except:
            return abort(400, "Error. See if prev_news_id is correctly given.")


        skip = 0 + 20 * ( page - 1)
        limit = 20

        # Request for recent articles, i.e. latest articles by scrap_time.
        # Store the mongo query in old.
        # Limit is set so that we don't perform a huge graph traversal.
        try:
            old = collection.find(
                {
                    "similarity_flag": 1,       #select  those with similarity and summary are generated
                    "summarized_flag": 1,       # and primary summary exist.
                    "primary_summary": { "$ne": None},
                    "$text":                            #using a text search gives more extensive search.
                    { 
                        "$search" : "covid covid19 coronavirus virus sars corona pandemic quarantine comorbidity comorbid" 
                    } 
                },      
                {"_id":1},
                sort = [("scrap_time", -1)],
                limit = 2500)
            old = [o["_id"] for o in old]
        except:
            return abort(501, "Server Error. Can't get 'old' data from DB. ")


        
        news_id_list = old

        try:
            news_list_cursor = get_news_by_rscore(news_id_list, skip, limit, gravity= 3)
        except:
            return abort(501, "Server Error. Error in querying news list. Check DB. ")
 
 
 
        news_list = []

        for news in news_list_cursor:
            news["_id"] = str(news["_id"])
            if news["_id"] not in prev_news_id:
                news["similar_id"] = [str(n_id) for n_id in news["similar_id"]]
                news_list.append(news)

        
        retJSON = {
            "metadata":{
                "page":page,
                "news_count": len(news_list)
            },
            "news": news_list
        }
        #print(retJSON)
        return jsonify(retJSON)  



class Stock(Resource):
    def get(self):
        
        #check authentication
        if not authenticate_check(request):
            retJSON = {
                'error': "Invalid Authentication."
            }
            return jsonify(retJSON)     

        # get page for skip and limit
        try:
            page = int(request.args['page'])
        except:
            return abort(400, "Error. See if page number is correctly given.")


        try:
            prev_news_id = request.args.getlist('prev_news_id')

            if not isinstance(prev_news_id, list):
                abort(400, "Error. See if prev_news_id is array or list [].")
        except:
            return abort(400, "Error. See if prev_news_id is correctly given.")


        #Only choosing stock related topics
        subtopic_name = ["money", "stock", "market", "economy", "sensex"]#topic_subtopic_map.get("Business & Finance")       
        if subtopic_name == None:
            return abort(400, "Error in topic name. See if Topic Name is correctly given.")




        skip = 0 + 20 * ( page - 1)
        limit = 20

        # Request for recent articles, i.e. latest articles by scrap_time.
        # Store the mongo query in old.
        # Limit is set so that we don't perform a huge graph traversal.
        try:
            old = collection.find(
                {
                    "similarity_flag": 1,       #select  those with similarity and summary are generated
                    "summarized_flag": 1,       # and primary summary exist.
                    "primary_summary": { "$ne": None},
                    "subtopics": { "$in" : subtopic_name }
                },      
                {"_id":1, "similar_id": 1},
                sort = [("scrap_time", -1)],
                limit = 2500)
            old = [o for o in old]
        except:
            return abort(501, "Server Error. Can't get given subtopic data from DB. ")



        #Graph and unique id list
        try:
            G = create_graph(old)
        except:
            return abort(501, "Server Error. Error in Graph creation ")

        try:
            unique_id_list = get_unique_news_id(G)
        except:
            return abort(501, "Server Error. Error in getting connected components. ")


        try:
            news_list_cursor = get_news_by_rscore(unique_id_list, skip, limit)
        except:
            return abort(501, "Server Error. Error in querying news list. Check DB. ")
 
 
        news_list = []

        for news in news_list_cursor:
            news["_id"] = str(news["_id"])
            if news["_id"] not in prev_news_id:
                news["similar_id"] = [str(n_id) for n_id in news["similar_id"]]
                news_list.append(news)

        
        retJSON = {
            "metadata":{
                "page":page,
                "news_count": len(news_list)
            },
            "news": news_list
        }
        #print(retJSON)
        return jsonify(retJSON)    


class World(Resource):
    def get(self):
        
        #check authentication
        if not authenticate_check(request):
            retJSON = {
                'error': "Invalid Authentication."
            }
            return jsonify(retJSON)     

        # get page for skip and limit
        try:
            page = int(request.args['page'])
        except:
            return abort(400, "Error. See if page number is correctly given.")

        try:
            prev_news_id = request.args.getlist('prev_news_id')

            if not isinstance(prev_news_id, list):
                abort(400, "Error. See if prev_news_id is array or list [].")
        except:
            return abort(400, "Error. See if prev_news_id is correctly given.")

        #Only choosing World related topics
        subtopic_name = topic_subtopic_map.get("World")       
        if subtopic_name == None:
            return abort(400, "Error in World. See if Topic Name is correctly given or check mapping.")




        skip = 0 + 20 * ( page - 1)
        limit = 20

        # Request for recent articles, i.e. latest articles by scrap_time.
        # Store the mongo query in old.
        # Limit is set so that we don't perform a huge graph traversal.
        try:
            old = collection.find(
                {
                    "similarity_flag": 1,       #select  those with similarity and summary are generated
                    "summarized_flag": 1,       # and primary summary exist.
                    "primary_summary": { "$ne": None},
                    "subtopics": { "$in" : subtopic_name }
                },      
                {"_id":1, "similar_id": 1},
                sort = [("scrap_time", -1)],#[("scrap_time", -1)],
                limit = 2500)
            old = [o for o in old]
        except:
            return abort(501, "Server Error. Can't get given subtopic data from DB. ")



        #Graph and unique id list
        try:
            G = create_graph(old)
        except:
            return abort(501, "Server Error. Error in Graph creation ")

        try:
            unique_id_list = get_unique_news_id(G)
        except:
            return abort(501, "Server Error. Error in getting connected components. ")


        try:
            news_list_cursor = get_news_by_rscore(unique_id_list, skip, limit)
        except:
            return abort(501, "Server Error. Error in querying news list. Check DB. ")
 
 
        news_list = []

        for news in news_list_cursor:
            news["_id"] = str(news["_id"])
            if news["_id"] not in prev_news_id:
                news["similar_id"] = [str(n_id) for n_id in news["similar_id"]]
                news_list.append(news)

        
        retJSON = {
            "metadata":{
                "page":page,
                "news_count": len(news_list)
            },
            "news": news_list
        }
        #print(retJSON)
        return jsonify(retJSON)    



class Local(Resource):
    def get(self):
        
        #check authentication
        if not authenticate_check(request):
            retJSON = {
                'error': "Invalid Authentication."
            }
            return jsonify(retJSON)     

        # get page for skip and limit
        try:
            page = int(request.args.get('page'))

            #lng = request.args.get('lng')
            #lat = request.args.get('lat')

        except:
            return abort(400, "Error. See if page number is correctly given.")

        try:
            city = request.args['city']
            if city == "":
                city = None
        except:
            city = None
        
        try:
            state = request.args['state']
            if state == "":
                state = None
        except: 
            state = None


        try:
            prev_news_id = request.args.getlist('prev_news_id')

            if not isinstance(prev_news_id, list):
                abort(400, "Error. See if prev_news_id is array or list [].")
        except:
            return abort(400, "Error. See if prev_news_id is correctly given.")


        if city == None and state == None:
            city = "India"
            


        #print("type: ", type(page))
        #print("page: ", page)

        skip = 0 + 20 * ( page - 1)
        limit = 20

        # Request for recent articles, i.e. latest articles by scrap_time.
        # Store the mongo query in old.
        # Limit is set so that we don't perform a huge graph traversal.

        #get list of news for city and state
        old_city = []
        old_state = []

        if city != None:
            city = "\"" + city + "\"" 
            #print(city)
            try:
                old = collection.find(
                    {
                        "similarity_flag": 1,       #select  those with similarity and summary are generated
                        "summarized_flag": 1,       # and primary summary exist.
                        "primary_summary": { "$ne": None},
                        "$text": 
                        { 
                            "$search" :  city
                        } 
                    },      
                    {"similar_id":1},
                    sort = [("scrap_time", -1)],
                    limit = 2500)
                old_city = [o for o in old]
            except:
                return abort(501, "Server Error. Can't get 'old_city' data from DB. ")

        if state != None:
            state = "\"" + state + "\"" 
            #print(state)
            try:
                old = collection.find(
                    {
                        "similarity_flag": 1,       #select  those with similarity and summary are generated
                        "summarized_flag": 1,       # and primary summary exist.
                        "primary_summary": { "$ne": None},
                        "$text": 
                        { 
                            "$search" : state
                        } 
                    },      
                    {"similar_id":1},
                    sort = [("scrap_time", -1)],
                    limit = 2500)
                old_state = [o for o in old]
            except:
                return abort(501, "Server Error. Can't get 'old_state' data from DB. ")

        # get list of both city and state related news
        #union
        old = old_city + old_state #list( set(old_city).union(set(old_state)) )

        try:
            G = create_graph(old)
        except:
            return abort(501, "Server Error. Error in Graph creation ")

        try:
            unique_id_list = get_unique_news_id(G)
        except:
            return abort(501, "Server Error. Error in getting connected components. ")


        #unique_id_list = old
 
        try:
            news_list_cursor = get_news_by_rscore(unique_id_list, skip, limit)
        except:
            return abort(501, "Server Error. Error in querying news list. Check DB. ")
 
 
 
        news_list = []

        for news in news_list_cursor:
            news["_id"] = str(news["_id"])
            if news["_id"] not in prev_news_id:
                news["similar_id"] = [str(n_id) for n_id in news["similar_id"]]
                news_list.append(news)

        
        retJSON = {
            "metadata":{
                "page":page,
                "news_count": len(news_list)
            },
            "news": news_list
        }
        #print(retJSON)
        return jsonify(retJSON)  




class Digest(Resource):
    def get(self):
        
        #check authentication
        if not authenticate_check(request):
            retJSON = {
                'error': "Invalid Authentication."
            }
            return jsonify(retJSON)     

        # get page for skip and limit
        try:
            page = int(request.args.get('page'))
        except:
            return abort(400, "Error. See if page number is correctly given.")

        #print("type: ", type(page))
        #print("page: ", page)

        skip = 0 + 1 * ( page - 1)
        limit = 1

        # Request for recent articles, i.e. latest articles by scrap_time.
        # Store the mongo query in old.
        # Limit is set so that we don't perform a huge graph traversal.
        try:
            digest_old = collection_digest.find(
                {},
                sort = [("digest_time", -1)],
                skip= skip,
                limit = limit
                )
            digest_old = [o for o in digest_old]
        except:
            return abort(501, "Server Error. Can't get 'old' data from DB. ")

        try:
            digest_time = digest_old[0]['digest_time']
        except:
            retJSON = {
            "metadata":{
                "page":page,
                "digest_time": None,
                "news_count": 0
            },
            "news": []
        }
            return jsonify(retJSON)



        news_id_list = []
        for n in digest_old[0]['news_id']:
            news_id_list.append(n["_id"])



        try:
            news_list_cursor = collection.find(
                { "_id" : { "$in" : news_id_list } }, # get news data from unique list.
                {
                    "title":1,              # choose field to return
                    "img_src": 1,
                    "primary_summary": 1,
                    "url":1,
                    "date":1,
                    "authors":1,
                    "source_name":1,
                    "likes_count":1,
                    "views_count":1,
                    "share_count":1,
                    "scrap_time":1,
                    "tags":1,
                    "subtopics":1,
                    "video_src":1,
                    'dislikes_count': 1,
                    'laughing_count': 1,
                    'surprise_count': 1,
                    'confuse_count': 1,
                    'straight_count': 1,
                    'angry_count': 1,
                    "comments_count":1,
                    "similar_id":1,
                    "topics":1,                
                },
                sort=[("likes_count", -1)],      #sort by scrap time for recent news.
                )
        except:
            return abort(501, "Server Error. Error in querying news list. Check DB. ")
 
 
        news_list = []

        for news in news_list_cursor:
            news["_id"] = str(news["_id"])
            news["similar_id"] = [str(n_id) for n_id in news["similar_id"]]
            news_list.append(news)

        
        retJSON = {
            "metadata":{
                "page":page,
                "digest_time": digest_time,
                "news_count": len(news_list)
            },
            "news": news_list
        }
        #print(retJSON)
        return jsonify(retJSON)  






class Topic_Feed(Resource):
    def get(self):
        
        #check authentication
        if not authenticate_check(request):
            retJSON = {
                'error': "Invalid Authentication."
            }
            return jsonify(retJSON)     

        # get page for skip and limit
        try:
            page = int(request.args['page'])
        except:
            return abort(400, "Error. See if page number is correctly given.")

        try:
            topic_name = request.args['topic_name']
        except:
            return abort(400, "Error. See if Topic Name is correctly given.")

        try:
            prev_news_id = request.args.getlist('prev_news_id')

            if not isinstance(prev_news_id, list):
                abort(400, "Error. See if prev_news_id is array or list [].")
        except:
            return abort(400, "Error. See if prev_news_id is correctly given.")



        if topic_name != None:
            subtopic_name = topic_subtopic_map.get(topic_name)       
            if subtopic_name == None:
                return abort(400, "Error in topic name. See if Topic Name is correctly given.")




        skip = 0 + 20 * ( page - 1)
        limit = 20

        # Request for recent articles, i.e. latest articles by scrap_time.
        # Store the mongo query in old.
        # Limit is set so that we don't perform a huge graph traversal.
        try:
            old = collection.find(
                {
                    "similarity_flag": 1,       #select  those with similarity and summary are generated
                    "summarized_flag": 1,       # and primary summary exist.
                    "primary_summary": { "$ne": None},
                    "subtopics": { "$in" : subtopic_name }
                },      
                {"_id":1},
                sort = [("scrap_time", -1)],
                limit = 2500)
            old = [o["_id"] for o in old]
        except:
            return abort(501, "Server Error. Can't get given subtopic data from DB. ")


        unique_id_list = old
 
        try:
            news_list_cursor = get_news_by_rscore(unique_id_list, skip, limit)
        except:
            return abort(501, "Server Error. Error in querying news list. Check DB. ")
 
 
 
        news_list = []

        for news in news_list_cursor:
            news["_id"] = str(news["_id"])
            if news["_id"] not in prev_news_id:
                news["similar_id"] = [str(n_id) for n_id in news["similar_id"]]
                news_list.append(news)

        
        retJSON = {
            "metadata":{
                "page":page,
                "news_count": len(news_list)
            },
            "news": news_list
        }
        #print(retJSON)
        return jsonify(retJSON)  





class Publisher_Feed(Resource):
    def get(self):
        
        #check authentication
        if not authenticate_check(request):
            retJSON = {
                'error': "Invalid Authentication."
            }
            return jsonify(retJSON)     

        # get page for skip and limit
        try:
            page = int(request.args['page'])        
        except:
            return abort(400, "Error. See if page number is correctly given.")

        try:
            publisher_name = request.args['publisher_name']
        except:
            return abort(400, "Error. See if Publisher Name is correctly given.")
        
        try:
            prev_news_id = request.args.getlist('prev_news_id')

            if not isinstance(prev_news_id, list):
                abort(400, "Error. See if prev_news_id is array or list [].")
        except:
            return abort(400, "Error. See if prev_news_id is correctly given.")




        if not isinstance(publisher_name, str):
                return abort(400, "Error in publisher name. See if Topic Name is correctly given as string.")




        skip = 0 + 20 * ( page - 1)
        limit = 20

        # Request for recent articles, i.e. latest articles by scrap_time.
        # Store the mongo query in old.
        # Limit is set so that we don't perform a huge graph traversal.
        try:
            news_list_cursor = collection.find(
                {
                    "similarity_flag": 1,       #select  those with similarity and summary are generated
                    "summarized_flag": 1,       # and primary summary exist.
                    "primary_summary": { "$ne": None},
                    "source_name": publisher_name
                },      
                {
                    "title":1,              # choose field to return
                    "img_src": 1,
                    "primary_summary": 1,
                    "url":1,
                    "date":1,
                    "authors":1,
                    "source_name":1,
                    "likes_count":1,
                    "views_count":1,
                    "share_count":1,
                    "scrap_time":1,
                    "tags":1,
                    "subtopics":1,
                    "video_src":1,
                    'dislikes_count': 1,
                    'laughing_count': 1,
                    'surprise_count': 1,
                    'confuse_count': 1,
                    'straight_count': 1,
                    'angry_count': 1,
                    "comments_count":1,
                    "similar_id":1,
                    "topics":1,
                },
                sort = [("_id", -1)],#[("scrap_time", -1)],
                skip = skip,
                limit = limit)
        except:
            return abort(501, "Server Error. Can't get given subtopic data from DB. ")


        #unique_id_list = old

 
 
        news_list = []

        for news in news_list_cursor:
            news["_id"] = str(news["_id"])
            if news["_id"] not in prev_news_id:
                news["similar_id"] = [str(n_id) for n_id in news["similar_id"]]
                news_list.append(news)

        
        retJSON = {
            "metadata":{
                "page":page,
                "news_count": len(news_list)
            },
            "news": news_list
        }
        #print(retJSON)
        return jsonify(retJSON)  





class Business(Resource):
    def get(self):
        
        #check authentication
        if not authenticate_check(request):
            retJSON = {
                'error': "Invalid Authentication."
            }
            return jsonify(retJSON)     

        # get page for skip and limit
        try:
            page = int(request.args['page'])
        except:
            return abort(400, "Error. See if page number is correctly given.")

        try:
            prev_news_id = request.args.getlist('prev_news_id')

            if not isinstance(prev_news_id, list):
                abort(400, "Error. See if prev_news_id is array or list [].")
        except:
            return abort(400, "Error. See if prev_news_id is correctly given.")


        #Only choosing business related topics
        subtopic_name = ["business", "banking", "economy", "companies", "industry", "infrastructure", "start-ups"]#topic_subtopic_map.get("Business & Finance")       
        if subtopic_name == None:
            return abort(400, "Error in topic name. See if Topic Name is correctly given.")




        skip = 0 + 20 * ( page - 1)
        limit = 20

        # Request for recent articles, i.e. latest articles by scrap_time.
        # Store the mongo query in old.
        # Limit is set so that we don't perform a huge graph traversal.
        try:
            old = collection.find(
                {
                    "similarity_flag": 1,       #select  those with similarity and summary are generated
                    "summarized_flag": 1,       # and primary summary exist.
                    "primary_summary": { "$ne": None},
                    "subtopics": { "$in" : subtopic_name }
                },      
                {"_id":1, "similar_id": 1},
                sort = [("scrap_time", -1)],
                limit = 2500)
            old = [o for o in old]
        except:
            return abort(501, "Server Error. Can't get given subtopic data from DB. ")



        #Graph and unique id list
        try:
            G = create_graph(old)
        except:
            return abort(501, "Server Error. Error in Graph creation ")

        try:
            unique_id_list = get_unique_news_id(G)
        except:
            return abort(501, "Server Error. Error in getting connected components. ")


        try:
            news_list_cursor = get_news_by_rscore(unique_id_list, skip, limit)
        except:
            return abort(501, "Server Error. Error in querying news list. Check DB. ")
 
 
        news_list = []

        for news in news_list_cursor:
            news["_id"] = str(news["_id"])
            if news["_id"] not in prev_news_id:
                news["similar_id"] = [str(n_id) for n_id in news["similar_id"]]
                news_list.append(news)

        
        retJSON = {
            "metadata":{
                "page":page,
                "news_count": len(news_list)
            },
            "news": news_list
        }
        #print(retJSON)
        return jsonify(retJSON)  


class Publishers_List(Resource):
    def get(self):
        '''
            Return publishers list with their logo.
        '''
        #check authentication
        if not authenticate_check(request):
            retJSON = {
                'error': "Invalid Authentication."
            }
            return jsonify(retJSON)     

        df = read_csv('./publisher_file.csv')

        publisher_json = df.to_json(orient='records' )

        retJSON = {
            "publishers_list": json.loads(publisher_json)
        }
        return retJSON
        
    

class Topic_List(Resource):
    def get(self):
        '''
            Return Topic list.
        '''
        #check authentication
        if not authenticate_check(request):
            retJSON = {
                'error': "Invalid Authentication."
            }
            return jsonify(retJSON)     

        # directly load the topic subtopic map 
        retJSON = sect_topic_image_map

        return retJSON
        

class Section_Topic_List(Resource):
    def get(self):
        '''
            Return Section, Topic list.
        '''
        #check authentication
        if not authenticate_check(request):
            retJSON = {
                'error': "Invalid Authentication."
            }
            return jsonify(retJSON)     

        # directly load the topic subtopic map 
        retJSON = sect_topic_subtopic_map

        return retJSON


class For_You(Resource):
    def post(self):
        # write what to do for post request and Add class
        #Load the data
        postedData = request.get_json()

        #check authentication
        if not authenticate_check(request):
            retJSON = {
                'error': "Invalid Authentication."
            }
            return jsonify(retJSON)     

        #Validation
        try:
            history_news_ids = postedData['history_news_ids']
        except:
            return abort(400, "Error in history_news_ids list. Enter array [].")
        
        try:
            history_search = postedData['history_search']
        except:
            return abort(400, "Error in history_search list. Enter array [].")

        try:
            page =  int(postedData['page'])
        except:
            return abort(400, "Error. See if page number is correctly given.")


        try:
            prev_news_id = postedData.get('prev_news_id')

            if not isinstance(prev_news_id, list):
                #abort(400, "Error. See if prev_news_id is array or list [].")
                prev_news_id = []

        except:
            return abort(400, "Error. See if prev_news_id is correctly given.")

        #redirect if not enough data
        #if len(history_news_ids) < 3:
        #    return redirect("/hot?page=" + str(np.random.randint(1,6)))

        if len(history_news_ids) < 2:
            page = np.random.randint(1,6)
            retJSON = RecentNews_helper(page)

            return retJSON






        skip = 0 + 20 * ( page - 1)
        limit = 20

        try:
            history_news_ids = [ObjectId(id) for id in history_news_ids]
        except:
            return abort(501, "Error in converting to BSON Object ID.")
            

        try:
            old = collection.find(
                {
                    "similarity_flag": 1,       #select  those with similarity and summary are generated
                    "summarized_flag": 1,       # and primary summary exist.
                    "primary_summary": { "$ne": None},
                    "_id": {"$in": history_news_ids}
                },      
                {"_id":1, "tags": 1})


            history_news_tags = set()

            #get the tags from history_id
            for o in old:
                history_news_tags.update(o["tags"])
        except:
            return abort(501, "Server Error. Can't get given 'history_news_ids' data from DB. ")


        #add those keywords in array with history search
        try:
            history_news_tags.update(history_search)
            history_news_string = remove_stopwords(' '.join(history_news_tags))
        except:
            return abort(501, "Server Error. Error in removing stopwords. ")

        try:
            old = collection.find(
                {
                    "similarity_flag": 1,       #select  those with similarity and summary are generated
                    "summarized_flag": 1,       # and primary summary exist.
                    "primary_summary": { "$ne": None},
                    "$text":                            #using a text search gives more extensive search.
                    { 
                        "$search" : history_news_string
                    } 
                },      
                {
                    "similar_id":1,
                    "score": { "$meta": "textScore" }
                },
                sort = [('score', {'$meta': 'textScore'})],
                limit = 2500)
            old = [o for o in old]
        except:
            return abort(501, "Server Error. Can't get given 'history_news_string' data from DB. ")
        

        try:
            G = create_graph(old)
        except:
            return abort(501, "Server Error. Error in Graph creation ")

        try:
            unique_id_list = get_unique_news_id(G)
        except:
            return abort(501, "Server Error. Error in getting connected components. ")


        try:
            news_list_cursor = get_news_by_rscore(unique_id_list, skip, limit, gravity=1.04) #1.04 to allow more older post
            # news_list_cursor = collection.find(
            #     {
            #         "_id": {"$in" : unique_id_list},
            #         "$text":                            #using a text search gives more extensive search.
            #         { 
            #             "$search" : history_news_string
            #         } 
            #     },      
            #     {
            #         "title":1,              # choose field to return
            #         "img_src": 1,
            #         "primary_summary": 1,
            #         "url":1,
            #         "date":1,
            #         "authors":1,
            #         "source_name":1,
            #         "likes_count":1,
            #         "views_count":1,
            #         "share_count":1,
            #         "scrap_time":1,
            #         "tags":1,
            #         "subtopics":1,
            #         "video_src":1,                 
            #         "score": { "$meta": "textScore" }
            #"comments_count":1,
            #"similar_id":1,
            #"topics":1,
            #     },
            #     sort = [('score', {'$meta': 'textScore'})],
            #     skip = skip,
            #     limit = limit)          
        except:
            return abort(501, "Server Error. Error in querying news list. Check DB. ")
 
 
        news_list = []

        for news in news_list_cursor:
            news["_id"] = str(news["_id"])
            if news["_id"] not in prev_news_id:
                news["similar_id"] = [str(n_id) for n_id in news["similar_id"]]
                news_list.append(news)

        
        retJSON = {
            "metadata":{
                "page":page,
                "news_count": len(news_list)
            },
            "news": news_list
        }
        #print(retJSON)
        return jsonify(retJSON)  
        

class News_By_Id(Resource):
    def post(self):
        # write what to do for post request and Add class
        #Load the data
        #check authentication
        if not authenticate_check(request):
            retJSON = {
                'error': "Invalid Authentication."
            }
            return jsonify(retJSON)     


        postedData = request.get_json()
        
                #Validation
        try:
            news_id_list = postedData['news_id_list']
        except:
            return abort(400, "Error in news_id_list list. Enter array [].")
        
        #convert string BSON
        try:
            news_id_list = [ObjectId(id) for id in news_id_list]
        except:
            return abort(501, "Error in converting to BSON Object ID.")

        #Get the projection as given in parameters
        try:
            unwanted = ["news_id_list"]
            for unwanted_key in unwanted: 
                del postedData[unwanted_key]

            proj = postedData
        except:
            return abort(400, "Error. See if the projection parameters are correct.")

        
        try:
            news_list_cursor = collection.find(
                {
                    "similarity_flag": 1,       #select  those with similarity and summary are generated
                    "summarized_flag": 1,       # and primary summary exist.
                    "primary_summary": { "$ne": None},
                    "_id": {"$in": news_id_list}
                },      
                projection= proj )
        except:
            return abort(501, "Server Error. Can't get given 'news_id_list' data from DB. ")

        news_list = []
        news_dict = {}


        for news in news_list_cursor:
            news["_id"] = str(news["_id"])
            try:
                news["similar_id"] = [str(n_id) for n_id in news["similar_id"]]
            except:
                pass
            news_dict[news["_id"]] = news

        #create a output list in the order as given in request
        for news_id in news_id_list:
            try:
                news_list.append(news_dict[str(news_id)])
            except:
                pass

        
        retJSON = {
            "metadata":{
                "news_count": len(news_list)
            },
            "news": news_list
        }
        #print(retJSON)
        return jsonify(retJSON)  




class Video_By_Id(Resource):
    def post(self):
        # write what to do for post request and Add class
        #Load the data
        #check authentication
        if not authenticate_check(request):
            retJSON = {
                'error': "Invalid Authentication."
            }
            return jsonify(retJSON)     


        postedData = request.get_json()
        
                #Validation
        try:
            video_id_list = postedData['video_id_list']
        except:
            return abort(400, "Error in video_id_list list. Enter array [].")
        
        #convert string BSON
        try:
            video_id_list = [ObjectId(id) for id in video_id_list]
        except:
            return abort(501, "Error in converting to BSON Object ID.")

        #Get the projection as given in parameters
        try:
            unwanted = ["video_id_list"]
            for unwanted_key in unwanted: 
                del postedData[unwanted_key]

            proj = postedData
        except:
            return abort(400, "Error. See if the projection parameters are correct.")

        
        try:
            news_list_cursor = db.youtube_video.find(
                {
                    "_id": {"$in": video_id_list}
                },      
                projection= proj )
        except:
            return abort(501, "Server Error. Can't get given 'video_id_list' data from DB. ")


        video_list = []
        video_dict = {}


        for news in news_list_cursor:
            news["_id"] = str(news["_id"])
            try:
                news["similar_id"] = [str(n_id) for n_id in news["similar_id"]]
            except:
                pass
            video_dict[news["_id"]] = news

        #create a output list in the order as given in request
        for video_id in video_id_list:
            try:
                video_list.append(video_dict[str(video_id)])
            except:
                pass

        
        retJSON = {
            "metadata":{
                "news_count": len(video_list)
            },
            "news": video_list
        }
        #print(retJSON)
        return jsonify(retJSON)  



class Polls_By_Id(Resource):
    def post(self):
        #Load the data
        #check authentication
        if not authenticate_check(request):
            retJSON = {
                'error': "Invalid Authentication."
            }
            return jsonify(retJSON)     


        postedData = request.get_json()
        
        #Validation
        try:
            polls_id_list = postedData['polls_id_list']
        except:
            return abort(400, "Error in polls_id_list list. Enter array [].")
        
        #convert string BSON
        try:
            polls_id_list = [ObjectId(id) for id in polls_id_list]
        except:
            return abort(501, "Error in converting to BSON Object ID.")

        #Get the projection as given in parameters
        try:
            unwanted = ["polls_id_list"]
            for unwanted_key in unwanted: 
                del postedData[unwanted_key]

            proj = postedData
        except:
            return abort(400, "Error. See if the projection parameters are correct.")

        
        try:
            news_list_cursor = db.polls.find(
                {
                    "_id": {"$in": polls_id_list}
                },      
                projection = proj)
        except:
            return abort(501, "Server Error. Can't get given 'polls_id_list' data from DB. ")


        polls_list = []
        polls_dict = {}


        for news in news_list_cursor:
            news["_id"] = str(news["_id"])
            try:
                news["similar_id"] = [str(n_id) for n_id in news["similar_id"]]
            except:
                pass
            polls_dict[news["_id"]] = news

        #create a output list in the order as given in request
        for polls_id in polls_id_list:
            try:
                polls_list.append(polls_dict[str(polls_id)])
            except:
                pass

        
        retJSON = {
            "metadata":{
                "news_count": len(polls_list)
            },
            "polls": polls_list
        }
        #print(retJSON)
        return jsonify(retJSON)



class UserAction(Resource):
    def get(self):

        #check authentication
        if not authenticate_check(request):
            retJSON = {
                'error': "Invalid Authentication."
            }
            return jsonify(retJSON)             
        
        try:
            action_name = request.args['action_name']

            if action_name not in ["likes_count","views_count", "share_count",'dislikes_count','laughing_count','surprise_count','confuse_count','straight_count','angry_count']:
                raise Exception('check action type')
        except Exception as e:
            return abort(400, "Error. See if Action Name and its type is correctly given.")    

        try:
            news_id = request.args['news_id']
        except:
            return abort(400, "Error. See if News _id is correctly given.")

        try:
            value = request.args['value']
            value = int(value)
            if not (value == 1 or value == -1):
                raise Exception('check value')

        except Exception as e:
            return abort(400, "Error. See if value is correctly given. Allowed values (-1, 1).")    
        

        news_id = ObjectId(news_id)

        if value == -1:
            try:
                action = collection.find_one(
                    {"_id": news_id},
                    {action_name : 1}
                    )
                
                action_count = action[action_name]

                if action_count - 1 >= 0 :
                    collection.update_one(
                        {"_id": news_id},
                        { "$inc": { action_name : -1 } }
                        )
            except:
                return abort(400, "Error in updating value (-1). ") 
        elif value == 1:
            try:
                collection.update_one(
                    {"_id": news_id},
                    { "$inc": { action_name : 1 } }
                    )
            except:
                return abort(400, "Error in updating value (+1). ") 

        retJSON = {
            "incremented": action_name,
            'value': value
        }

        return jsonify(retJSON)


class PostComment(Resource):
    def post(self):

        #check authentication
        if not authenticate_check(request):
            retJSON = {
                'error': "Invalid Authentication."
            }
            return jsonify(retJSON)             

        postedData = request.get_json()

        try:
            news_id = postedData['news_id']
            news_id = ObjectId(news_id)
        except:
            return abort(400, "Error. See if News _id is given correctly.")   

        try:
            user_id = postedData['user_id']
        except:
            return abort(400, "Error. See if User_id is given correctly.")   

        try:
            comment_msg = postedData['comment_msg']
            if len(comment_msg) < 1:
                raise Exception('too short comment_msg')
        except Exception as e:
            return abort(400, repr(e) + "\nError. See if comment_msg is given correctly. comment_msg can't be empty.")    


        try:
            user_name = postedData['user_name']
        except:
            abort(400, "Error. See if user_name is given correctly.")   


        try:
            collection.update_one(
                {"_id": news_id},
                {
                    "$push": {
                        "comments": { 
                            "comment_msg": comment_msg,
                            "news_id": str(news_id),
                            "user_id": user_id,
                            "user_name": user_name,
                            "comment_time": int(time.time())
                            }
                        },
                    "$inc": { "comments_count": 1}
                }
                )
        except:
            return abort(400, "Error. Can't update the comment.")

        retJSON = {
            "message": "Success."
        }

        return jsonify(retJSON)


class GetComment(Resource):
    def get(self):
        #check authentication
        if not authenticate_check(request):
            retJSON = {
                'error': "Invalid Authentication."
            }
            return jsonify(retJSON)             
        try:
            news_id = request.args['news_id']
            news_id = ObjectId(news_id)
        except Exception as e:
            return abort(400, "Error. See if news _id is given correctly.")    

        # try:
        #     page = int(request.args['page'])
        # except:
        #     return abort(400, "Error. See if page is given correctly.")


        try:
            comments_list = collection.find_one(
                {"_id": news_id},
                {"comments": 1}
            )
            comments_list = comments_list['comments']
            comments_list.reverse()
        except:
            return abort(400, "Error. Can't get the comments.")

        retJSON = {
            "comments_count" : len(comments_list),
            "comments": comments_list
        }
          
        return jsonify(retJSON)




class Polls(Resource):
    def get(self):
        #check authentication
        if not authenticate_check(request):
            retJSON = {
                'error': "Invalid Authentication."
            }
            return jsonify(retJSON)     

        try:
            page = int(request.args['page'])
        except:
            return abort(400, "Error. See if page number is correctly given.")


        skip = 0 + 10 * ( page - 1)
        limit = 10

        try:
            polls_cursor = db.polls.find(
                {},
                sort = [("scrap_time", -1)],
                skip = skip,
                limit = limit
                )

        except:
            abort(400, "Error. Can't fetch the polls.")

        polls_list = []
        for poll in polls_cursor:
            poll["_id"]  = str(poll["_id"])
            polls_list.append(poll)


        retJSON = {
            "metadata":{
                "page":page,
                "news_count": len(polls_list)
            },
            "polls": polls_list
        }

        return jsonify(retJSON)


class Current_Polls(Resource):
    def get(self):
        #check authentication
        if not authenticate_check(request):
            retJSON = {
                'error': "Invalid Authentication."
            }
            return jsonify(retJSON)     

        try:
            page = int(request.args['page'])
        except:
            return abort(400, "Error. See if page number is correctly given.")


        skip = 0 + 10 * ( page - 1)
        limit = 10


        hours = 7*24
        cur_time = int(time.time())
        time_gap = 60*60*hours # hours in seconds

        try:
            polls_cursor = db.polls.find(
                {"scrap_time": {"$gte": cur_time - time_gap  }},
                sort = [("scrap_time", -1)],
                skip = skip,
                limit = limit      
                )

        except:
            abort(400, "Error. Can't fetch the polls.")

        polls_list = []
        for poll in polls_cursor:
            poll["_id"]  = str(poll["_id"])
            polls_list.append(poll)


        retJSON = {
            "metadata":{
                "page":page,
                "news_count": len(polls_list)
            },
            "polls": polls_list
        }

        return jsonify(retJSON)

class Previous_Polls(Resource):
    def get(self):
        #check authentication
        if not authenticate_check(request):
            retJSON = {
                'error': "Invalid Authentication."
            }
            return jsonify(retJSON)     

        try:
            page = int(request.args['page'])
        except:
            return abort(400, "Error. See if page number is correctly given.")


        skip = 0 + 10 * ( page - 1)
        limit = 10


        hours = 7*24
        cur_time = int(time.time())
        time_gap = 60*60*hours # hours in seconds

        try:
            polls_cursor = db.polls.find(
                {"scrap_time": {"$lt": cur_time - time_gap  }},
                sort = [("scrap_time", -1)],
                skip = skip,
                limit = limit      
                )

        except:
            abort(400, "Error. Can't fetch the polls.")

        polls_list = []
        for poll in polls_cursor:
            poll["_id"]  = str(poll["_id"])
            polls_list.append(poll)


        retJSON = {
            "metadata":{
                "page":page,
                "news_count": len(polls_list)
            },
            "polls": polls_list
        }

        return jsonify(retJSON)


class PollsAction(Resource):
    def get(self):
        #check authentication
        if not authenticate_check(request):
            retJSON = {
                'error': "Invalid Authentication."
            }
            return jsonify(retJSON)             
        
        try:
            action_name = request.args['action_name']
        except Exception as e:
            return abort(400, "Error. See if Action Name and its type is correctly given.")    

        try:
            poll_id = request.args['poll_id']
        except:
            return abort(400, "Error. See if poll_id is correctly given.")

        try:
            value = request.args['value']
            value = int(value)
            if not (value == 1 or value == -1):
                raise Exception('check value')

        except Exception as e:
            return abort(400, "Error. See if value is correctly given. Allowed values (-1, 1).")    
        

        poll_id = ObjectId(poll_id)

        if value == -1:
            try:
                action = db.polls.find_one(
                    {"_id": poll_id},
                    {"options" : 1}
                    )

                action_count = action["options"][action_name]

                if action_count - 1 >= 0 :
                    db.polls.update_one(
                        {"_id": poll_id},
                        { 
                            "$inc": { "options."+action_name : -1, "total_actions" : -1 }
                        }
                        )
            except:
                return abort(400, "Error in updating value (-1). Maybe check action_name.") 
        elif value == 1:
            try:
                db.polls.update_one(
                    {"_id": poll_id},
                    { 
                        "$inc": { "options."+action_name : 1, "total_actions" : 1 }
                    }
                    )
            except:
                return abort(400, "Error in updating value (+1). Maybe check action_name. ") 

        retJSON = {
            "incremented": action_name,
            'value': value
        }

        return jsonify(retJSON)        
        



class Search(Resource):
    def get(self):
        #check authentication
        if not authenticate_check(request):
            retJSON = {
                'error': "Invalid Authentication."
            }
            return jsonify(retJSON)             

        # get page for skip and limit
        try:
            page = int(request.args.get('page'))

        except:
            return abort(400, "Error. See if page number is correctly given.")

        try:
            s_string = request.args.get('s_string')
        except:
            return abort(400, "Error. See if the search string is given.")


        #print("type: ", type(page))
        #print("page: ", page)

        skip = 0 + 20 * ( page - 1)
        limit = 20

        # Request for recent articles, i.e. latest articles by scrap_time.
        # Store the mongo query in old.
        # Limit is set so that we don't perform a huge graph traversal.


        try:
            news_list_cursor = collection.find(
                {
                    "similarity_flag": 1,       #select  those with similarity and summary are generated
                    "summarized_flag": 1,       # and primary summary exist.
                    "primary_summary": { "$ne": None},
                    "$text": 
                    { 
                        "$search" :  s_string
                    }
                },      
                {
                "title":1,              # choose field to return
                "img_src": 1,
                "primary_summary": 1,
                "url":1,
                "date":1,
                "authors":1,
                "source_name":1,
                "likes_count":1,
                "views_count":1,
                "share_count":1,
                "scrap_time":1,
                "tags":1,
                "subtopics":1,
                "video_src":1,
                'dislikes_count': 1,
                'laughing_count': 1,
                'surprise_count': 1,
                'confuse_count': 1,
                'straight_count': 1,
                'angry_count': 1,
                "comments_count":1,
                "similar_id":1,
                "topics":1,
                "score": { "$meta": "textScore" }                     
                },
                sort = [("_id", -1)],#[('score', {'$meta': 'textScore'}), ("scrap_time", -1)],
                skip = skip,
                limit = limit)
        except:
            return abort(501, "Server Error. Can't get data from DB. ")
 
 
        news_list = []

        for news in news_list_cursor:
            news["_id"] = str(news["_id"])
            news["similar_id"] = [str(n_id) for n_id in news["similar_id"]]
            news_list.append(news)

        
        retJSON = {
            "metadata":{
                "page":page,
                "news_count": len(news_list)
            },
            "news": news_list
        }
        #print(retJSON)
        return jsonify(retJSON)  


class CovidTimeline(Resource):
    def get(self):
        #check authentication
        if not authenticate_check(request):
            retJSON = {
                'error': "Invalid Authentication."
            }
            return jsonify(retJSON)     
        try:
            cursor = db.covid_stats.find({}, {"name":1, "data":1, "_id":0})
        except:
            return abort(500, "Error. Can't access the db.covid_stats.")
            
            #docs = [doc for doc in cursor]
        try:
            retJSON = {}
            for doc in cursor:
                retJSON[doc["name"]] = doc

            return jsonify(retJSON)
        except:
            return abort(500, "Error. Check the documents in covid_stats.")

class CovidShareCount(Resource):
    """To increment the covid share count."""
    def get(self):
        #check authentication
        if not authenticate_check(request):
            retJSON = {
                'error': "Invalid Authentication."
            }
            return jsonify(retJSON)     
            
        try:
            db.covid_stats.update_one(
                {"name": "Share_Covid_Count"},
                { "$inc": { "data" : 1 } }
                )

            retJSON = {
                "message": "success"
            }

            return jsonify(retJSON)
        except:
            return abort(500, "Error. Can't access the db.covid_stats.")
            
            #docs = [doc for doc in cursor]



#Endpoints
api.add_resource(Home, '/home')
api.add_resource(RecentNews, '/recentnews')
api.add_resource(Hot, '/hot')
api.add_resource(Following, '/following')
api.add_resource(Video, '/video')
api.add_resource(Video_2, '/video_2')


api.add_resource(Covid, '/covid')
api.add_resource(Stock, '/stock')
api.add_resource(Local, '/local')
api.add_resource(Digest, '/digest')
api.add_resource(Business, '/business')
api.add_resource(For_You, '/foryou')
api.add_resource(World, '/world')




api.add_resource(Topic_Feed, '/topic_feed')
api.add_resource(Publisher_Feed, '/publisher_feed')
api.add_resource(Publishers_List, '/publishers_list')
api.add_resource(Topic_List, '/topic_list')
api.add_resource(Section_Topic_List, '/section_list')


api.add_resource(News_By_Id, '/news_by_id')
api.add_resource(Video_By_Id, '/video_by_id')


api.add_resource(UserAction, '/useraction')
api.add_resource(PostComment, '/postcomment')
api.add_resource(GetComment, '/getcomment')

api.add_resource(Polls, '/polls')
api.add_resource(Current_Polls, '/current_polls')
api.add_resource(Previous_Polls, '/previous_polls')
api.add_resource(Polls_By_Id, '/polls_by_id')


api.add_resource(PollsAction, '/pollsaction')

api.add_resource(Search, '/search')
api.add_resource(CovidTimeline, '/covidtimeline')
api.add_resource(CovidShareCount, '/covidsharecount')























if __name__ == '__main__':
    app.run()#host='0.0.0.0'